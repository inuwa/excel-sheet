import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';

@Component({
    selector: 'lib-cell',
    templateUrl: './cell.component.html',
    styleUrls: ['./cell.component.scss'],
})
export class CellComponent implements OnInit {
    _disabled: boolean = true;
    @ViewChild('ionInput') ionInput: IonInput
    constructor() { }
    ngOnInit() { }

    get disabled() {
        return this._disabled;
    }

    set disabled(y) {
        this._disabled = y;
        this.setAutoFocus();
    }

    setAutoFocus() {
        setTimeout(() => {
            this.ionInput.setFocus();
        });
    }
}
