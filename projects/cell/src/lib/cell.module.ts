import { NgModule } from '@angular/core';
import { CellComponent } from './cell.component';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [CellComponent],
    imports: [
        FormsModule,
        IonicModule
    ],
    exports: [CellComponent]
})
export class CellModule { }
