import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MainPage } from './main.page';
import { HeaderComponent } from '../header/header.component';
import { CellModule } from 'projects/cell/src/public-api';

const routes: Routes = [
    {
        path: '',
        component: MainPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        CellModule
    ],
    declarations: [MainPage, HeaderComponent],
    exports: [HeaderComponent]
})
export class MainPageModule { }
