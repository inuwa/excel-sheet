import { Component, OnInit, ViewChildren, QueryList, HostListener, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { CellComponent } from 'projects/cell/src/public-api';
import { CellService } from '../cell.service';

@Component({
    selector: 'app-main',
    templateUrl: './main.page.html',
    styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

    @ViewChildren(CellComponent) cellComponents: QueryList<CellComponent>;
    cells: any[] = [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    ];
    constructor(private cellService: CellService) { }

    ngOnInit() { }

    ngAfterViewInit() {
        setTimeout(() => {
            if (!this.cellService.cellReference) {
                this.cellComponents.toArray()[0].disabled = false;
                return;
            }
        }, 3);
    }

    $$entered(index: number, $event: KeyboardEvent) {
        if ($event.keyCode === 13) {
            console.log('Cell Clicked: ', index);
            this.cellService.cellReference = this.cellComponents.toArray()[index];
            this.cellService.cellReference.disabled = true;
            let offset = 5;
            console.log($event);

            if ($event.shiftKey) offset = -5;
            if ((index + offset) < 0) offset = 0;
            if ((index + offset) > this.cellComponents.length) offset
            this.cellService.cellReference = this.cellComponents.toArray()[index + offset];
        }
    }
}
