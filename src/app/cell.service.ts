import { Injectable } from '@angular/core';
import { CellComponent } from 'projects/cell/src/public-api';

@Injectable({
    providedIn: 'root'
})
export class CellService {
    private _cellReference: CellComponent = null;
    constructor() { }
    ngOnInit() {

    }

    set cellReference(cellReference: CellComponent) {
        this._cellReference = cellReference;
        this._cellReference.disabled = false;
    }

    get cellReference() {
        return this._cellReference;
    }
}
